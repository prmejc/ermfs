-module(mfc).
-export([start/1, tick/1,  enterTsu/3, eventListener/3, addController/3]).
 
tick(Layout, TSUs, Controllers) ->
    {Layout, processTSUs(Layout, TSUs, Controllers)}.

entryTsu(TSUs, TSU_ID, LOCATION) ->
    io:format("Entering TSU " ++ TSU_ID ++ " at " ++ LOCATION ++ "~n"),
    [ { TSU_ID, LOCATION} | TSUs].

processTSUs(_, [], _) -> 
    [];
processTSUs(Layout, [ Tsu | TsuTail], Controllers) ->
    [moveTsu(Layout, Tsu, Controllers) | processTSUs(Layout, TsuTail, Controllers)].

moveTsu(Layout, { TSU_ID, LOCATION }, Controllers ) -> 
    [ FirstNeighbour | _ ] = digraph:out_neighbours(Layout, LOCATION),
    { TSU_ID, FirstNeighbour}.


printTsusState([]) ->
    io:format("~n");
printTsusState([TSU | TSUtail]) ->
    printTSUState(TSU),
    printTsusState(TSUtail).

printTSUState(TSU) ->
    io:format("     TSU: ~p~n", [TSU]).

eventListener(Layout, TsuList, Controllers) ->
    receive
        { From, tick } ->
            { LayoutNext, TsuListNext } = tick( Layout, TsuList, Controllers ),
            From ! { LayoutNext, TsuListNext },
            eventListener( LayoutNext, TsuListNext, Controllers )
            ;
        { From, {enterTsu, TSU, LOCATION } } ->
            TsuListNext = entryTsu(TsuList, TSU, LOCATION),
            From ! { Layout, TsuListNext },
            eventListener( Layout, TsuListNext, Controllers )
            ;
        { From, {addController, Location, Controller } } ->
            NewControllers = [ Controller | Controllers],
            From ! { Layout, TsuList },
            eventListener( Layout, TsuList, NewControllers )
            ;
        terminate ->
            ok
    end.


start(Layout) ->
    spawn(mfc, eventListener, [Layout, [], []]).

tick(Pid) ->
    Pid ! {self(), tick},
    receive
        { _, TsuList} -> TsuList
    after 3000 ->
        timeout
    end.

enterTsu(Pid, TSU, LOCATION) ->
    Pid ! {self(), {enterTsu, TSU, LOCATION}},
    receive
        { _, TsuList} -> TsuList
    after 3000 ->
        timeout
    end.

addController(Pid, Location, Controller) ->
    Pid ! {self(), {addController, Location, Controller}},
    receive
        { _, TsuList} -> TsuList
    after 3000 ->
        timeout
    end.